'use strict'

const Opal = global.Opal

const EMPTY = Opal.hash2([], {})

const $precedingAttributes = Symbol('precedingAttributes')

const innerDocument = (() => {
  const scope = Opal.klass(
    Opal.module(null, 'Templates', function () {
    }),
    Opal.module(null, 'Asciidoctor').Document,
    'innerDocument',
    function () {
    }
  )
  Opal.defn(scope, '$initialize', function initialize (backend, opts, precedingAttributes) {
    Opal.send(this, Opal.find_super_dispatcher(this, 'initialize', initialize), [backend, opts])
    this[$precedingAttributes] = precedingAttributes
  })
  Opal.defn(scope, '$convert', function $$convert (opts) {
    var self = this
    self.parent_document.$playback_attributes(self[$precedingAttributes])
    return Opal.send(this, Opal.find_super_dispatcher(this, 'convert', $$convert), [opts])
  })
  return scope
})()

const attributeEntry = Opal.module(null, 'Asciidoctor').Document.AttributeEntry

function prepareAttributes (attributes, formats, item, parent) {
  //precedingAttributeEntries are the attributes defined between the previous block and this block
  const precedingAttributeEntries = attributes.attribute_entries
  delete attributes.attribute_entries
  const precedingAttributes = Opal.hash2(['attribute_entries'], { attribute_entries: precedingAttributeEntries })
  //Turn attributes into a hash suitable for playbackAttributes
  const attrHash = Opal.hash2(['attribute_entries'],
    {
      attribute_entries: Object.entries(attributes).map(([name, value]) => {
        return attributeEntry.$new(name, value, value === undefined)
      })
        .concat(Object.entries(formats).map(([name, formatFn]) => {
          const value = formatFn(item)
          return attributeEntry.$new(name, value, false)
        })),
    })

  parent.document.$playback_attributes(precedingAttributes)
  //Save attributes up to here, including preceding attributes
  const originalAttributes = parent.document.$attributes().$merge(EMPTY)
  //apply attributes for this template
  parent.document.$playback_attributes(attrHash)
  return { precedingAttributes, originalAttributes }
}

function applySubs (parent, attributes, template, type, name) {
  const subs = attributes.subs || 'attributes'
  template = parent.applySubstitutions(template, parent.$resolve_subs(subs, type, ['attributes'], name))
  return template
}

function makeInnerDoc (registry, parent, template, precedingAttributes, originalAttributes) {
  const options = Opal.hash2(['attributes', 'standalone', 'extension_registry', 'parent'],
    {
      standalone: false,
      extension_registry: registry,
      parent: parent.document,
    })
  const innerDoc = innerDocument.$new(template, options, precedingAttributes).parse()
  parent.document.attributes = originalAttributes
  return innerDoc
}

module.exports.doTemplate = (registry, template, parent, attributes, formats, item) => {
  const { precedingAttributes, originalAttributes } = prepareAttributes(attributes, formats, item, parent)
  template = applySubs(parent, attributes, template, 'block', 'jsonPathBlock')
  return makeInnerDoc(registry, parent, template, precedingAttributes, originalAttributes)
}
