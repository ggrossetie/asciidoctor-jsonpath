'use strict'

const Opal = global.Opal
const jp = require('jsonpath')
const staticEval = require('static-eval')
const parse = require('esprima').parse

const { computeParent, print } = require('./list-utils')
const { doTemplate } = require('./block')

const DEBUG = false

module.exports.register = function (registry, config = {}) {
  // For a per-page extension in Antora, config will have the structure:
  //{ file, // the vfs file being processed
  // contentCatalog, // the Antora content catalog
  // config // the asciidoc section of the playbook, enhanced with asciidoc attributes from the component descriptor.
  // }

  // For "required" global extensions in Antora, config will be undefined (replaced with {}).
  // Other loading code may use other config options.

  //File access

  const vfs = (config.vfs && typeof config.vfs.read === 'function')
    ? config.vfs
    //Antora support
    : (config.file && config.contentCatalog && typeof config.contentCatalog.resolveResource === 'function')
      ? {
        read: (resourceId) => {
          const target = config.contentCatalog.resolveResource(resourceId, config.file.src)
          if (target) return target.contents
          console.log(`could not resolve ${resourceId} in context of ${config.file.src}`)
          return undefined
        },
      }
      //look in the file system
      : fsAccess()

  function fsAccess () {
    const fs = require('fs')
    return {
      read: (path, encoding = 'utf8') => {
        if (path.startsWith('file://')) {
          return fs.readFileSync(path.substr('file://'.length), encoding)
        }
        return fs.readFileSync(path, encoding)
      },
    }
  }

  function jsonpathBlock () {
    const self = this
    self.named('jsonpathBlock')
    self.onContext(['listing', 'open', 'paragraph'])
    self.positionalAttributes(['target', 'query', 'formats'])
    self.process(function (parent, reader, attributes) {
      const template = reader.$read()

      const target = attributes.target
      const query = attributes.query
      const formatSpecs = attributes.formats ? attributes.formats.split(',') : []
      const formats = formatSpecs.reduce((accum, formatSpec) => {
        if (formatSpec.includes('=')) {
          const [name, exp] = formatSpec.split('=')
          const parsed = parse(exp).body[0].expression
          accum[name] = (item) => staticEval(parsed, item)
        } else {
          accum[formatSpec] = (item) => item[formatSpec]
        }
        return accum
      }, {})
      doQuery(target, query, (item) => {
        // console.log(`list item`, item.src)
        const innerDoc = doTemplate(registry, template, parent, attributes, formats, item)
        parent.blocks.push(innerDoc)
      })
    })
  }

  function jsonpathAttributesIncludeProcessor () {
    const self = this
    self.handles(function (target) {
      return target.startsWith('jsonpath$')
    })
    self.process(function (doc, reader, target, attributes) {
      target = target.slice(9)
      const query = attributes.query || attributes['1']
      const formatsString = attributes.formats || attributes['2']
      const formatSpecs = formatsString ? formatsString.split(',') : []
      const formats = formatSpecs.reduce((accum, formatSpec) => {
        if (formatSpec.includes('=')) {
          const [nameExp, exp] = formatSpec.split('=')
          const parsedNameExp = parse(nameExp).body[0].expression
          const parsedExp = parse(exp).body[0].expression
          accum.push([(item) => staticEval(parsedNameExp, item) || nameExp, (item) => staticEval(parsedExp, item)])
        } else {
          accum.push([() => formatSpec, (item) => item[formatSpec]])
        }
        return accum
      }, [])
      // console.log(`jsonpathAttributesMacro: target: ${target}, query: ${query}, formats: `, formats)
      doQuery(target, query, (item) => {
        // console.log(`list item`, item.src)
        setAttributes(formats, item, doc)
      })
    })
    function setAttributes (formats, item, doc) {
      const headerAttributes = doc.header_attributes
      const docAttributes = doc.attributes

      formats.forEach(([nameFn, formatFn]) => {
        const name = nameFn(item).toLowerCase()
        const value = formatFn(item)
        headerAttributes !== Opal.nil && headerAttributes['$[]='](name, value)
        docAttributes['$[]='](name, value)
      })
    }
  }

  function listProcessor (macroName, type, markerChar) {
    return function () {
      const self = this
      self.named(macroName)
      self.positionalAttributes(['query', 'format'])
      self.process((parent, target, attributes) => {
        // console.log('attrs', attrs)
        DEBUG && print(parent.document)
        const depth = attributes.level ? Number.parseInt(attributes.level) : undefined
        const list = computeParent(parent, depth, self, type, attributes.style)
        const marker = markerChar.repeat(depth || 1)

        const query = attributes.query
        const formatFn = formatFunction(attributes.format)

        doQuery(target, query, (item) => {
          // console.log(`list item`, item.src)
          const listItem = self.createListItem(list, formatFn(item))
          listItem.marker = marker
          list.blocks.push(listItem)
        })
        DEBUG && print(parent.document)
      })
    }
  }

  function jsonpathDescriptionListMacro () {
    const self = this
    self.named('jsonpathDescriptionList')
    self.positionalAttributes(['query', 'subjectformat', 'descriptionformat'])
    self.process((parent, target, attrs) => {
      // console.log('attrs', attrs)
      DEBUG && print(parent.document)
      const depth = attrs.level ? Number.parseInt(attrs.level) : undefined
      const list = computeParent(parent, depth, self, 'dlist', attrs.style)
      const query = attrs.query
      const subjectFormatFn = formatFunction(attrs.subjectformat)
      const descriptionFormatFn = formatFunction(attrs.descriptionformat)

      doQuery(target, query, (item) => {
        const listTerm = self.createListItem(list, subjectFormatFn(item))
        const listItem = self.createListItem(list, descriptionFormatFn(item))
        list.blocks.push([[listTerm], listItem])
      })
      DEBUG && print(parent.document)
    })
  }

  function jsonpathBlockTableMacro () {
    const self = this
    self.named('jsonpathTable')
    self.positionalAttributes(['query', 'cellformats'])
    self.process(function (parent, target, attributes) {
      const table = parent.blocks[parent.blocks.length - 1]
      if (table.context !== 'table') {
        console.warn(`indexTable block macro must follow a table, not ${table.context}`)
        return
      }
      const columns = table.columns
      const rows = table.rows.body
      const cellAttrs = attributes.cellformats ? attributes.cellformats.split(',') : []
      if (columns.length !== cellAttrs.length) {
        console.warn(`column count ${columns.length} differs from 'cellformats' attribute count ${cellAttrs.length}`)
        cellAttrs.length = columns.length
      }
      const cellFill = cellAttrs.map((cellAttr) => formatFunction(cellAttr))

      const query = attributes.query
      doQuery(target, query, (item) => {
        // console.log(`list item`, item.src)
        // console.log('attributes', attributes)
        const row = columns.map((col, i) =>
          Opal.Asciidoctor.Table.Cell.$new(col, cellFill[i](item) || ''))
        rows.push(row)
      })
    })
  }

  function jsonpathInlineCountMacro () {
    const self = this
    self.named('jsonpathCount')
    self.positionalAttributes(['query'])
    self.process(function (parent, target, attributes) {
      const query = attributes.query
      var count = 0
      doQuery(target, query, (item) => count++)
      return self.createInline(parent, 'quoted', count)
    })
  }

  function jsonpathInlineUniqueCountMacro () {
    const self = this
    self.named('jsonpathUniqueCount')
    self.positionalAttributes(['query', 'format'])
    self.process(function (parent, target, attributes) {
      const query = attributes.query
      const formatFn = formatFunction(attributes.format)
      var values = new Set()
      doQuery(target, query, (item) => values.add(formatFn(item)))
      return self.createInline(parent, 'quoted', values.size)
    })
  }

  function formatFunction (cellAttr) {
    if (cellAttr.includes('${')) {
      const expr = parse('`' + cellAttr + '`').body[0].expression
      return (item) => staticEval(expr, item)
    } else {
      return (item) => item[cellAttr]
    }
  }

  function doQuery (target, expr, fn) {
    const contents = vfs.read(target)
    const data = JSON.parse(contents.toString())
    const bits = expr.split('$')
    const method = bits[0] || 'query'
    expr = '$' + bits[1]
    const result = jp[method](data, expr)
    result.forEach((item) => fn(item))
  }

  function doRegister (registry) {
    if (typeof registry.block === 'function') {
      registry.block(jsonpathBlock)
    } else {
      console.warn('no \'block\' method on alleged registry')
    }
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(jsonpathBlockTableMacro)
      registry.blockMacro(jsonpathDescriptionListMacro)
      registry.blockMacro(listProcessor('jsonpathList', 'ulist', '*'))
      registry.blockMacro(listProcessor('jsonpathOrderedList', 'olist', '.'))
    } else {
      console.warn('no \'blockMacro\' method on alleged registry')
    }
    if (typeof registry.inlineMacro === 'function') {
      registry.inlineMacro(jsonpathInlineCountMacro)
      registry.inlineMacro(jsonpathInlineUniqueCountMacro)
    } else {
      console.warn('no \'inlineMacro\' method on alleged registry')
    }
    if (typeof registry.includeProcessor === 'function') {
      registry.prefer('include_processor', jsonpathAttributesIncludeProcessor)
    } else {
      console.warn('no \'includeProcessor\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}
