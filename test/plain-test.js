/* eslint-env mocha */
'use strict'

const jp = require('jsonpath')
const staticEval = require('static-eval')
var parse = require('esprima').parse
const { expect } = require('chai')

const data = {
  properties: [
    {
      name: 'foo',
      description: 'fooish',
    },
    {
      name: 'bar',
      description: 'barish',
    },
  ],
  object: {
    field1: {
      p1: 'a',
      p2: 'b',
    },
    field2: {
      p1: 'c',
      p2: 'd',
    },
    field3: {
      p1: 'e',
      p2: 'f',
    },
  },
}
it('basic', () => {
  const result = jp.query(data, '$.properties[*].name')
  expect(result).to.deep.equal(['foo', 'bar'])
})
it('expr', () => {
  const result = jp.query(data, '$.properties[0]')
  expect(result).to.deep.equal([{ name: 'foo', description: 'fooish' }])
})
it('object', () => {
  const result = jp.query(data, '$.object.*')
  expect(result).to.deep.equal([
    {
      p1: 'a',
      p2: 'b',
    },
    {
      p1: 'c',
      p2: 'd',
    },
    {
      p1: 'e',
      p2: 'f',
    },
  ])
})
it('object-nodes', () => {
  const result = jp.nodes(data, '$.object.*')
  expect(result).to.deep.equal([
    {
      path: [
        '$',
        'object',
        'field1',
      ],
      value: {
        p1: 'a',
        p2: 'b',
      },
    },
    {
      path: [
        '$',
        'object',
        'field2',
      ],
      value: {
        p1: 'c',
        p2: 'd',
      },
    },
    {
      path: [
        '$',
        'object',
        'field3',
      ],
      value: {
        p1: 'e',
        p2: 'f',
      },
    },
  ])
})

it('dynamic template on nodes item', () => {
  const item = {
    path: [
      '$',
      'object',
      'field1',
    ],
    value: {
      p1: 'a',
      p2: 'b',
    },
  }
  // eslint-disable-next-line no-template-curly-in-string
  const ast = parse('`${path[2]}`').body[0].expression
  const col1 = staticEval(ast, item)
  expect(col1).to.equal('field1')
})

it('static eval on nodes item', () => {
  const item = {
    path: [
      '$',
      'object',
      'field1',
    ],
    value: {
      p1: 'a',
      p2: 'b',
    },
  }
  const ast = parse('path[2]').body[0].expression
  const col1 = staticEval(ast, item)
  expect(col1).to.equal('field1')
})
