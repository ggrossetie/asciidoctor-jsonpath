/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const jsonpath = require('./../lib/jsonpath')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

describe('jsonpath tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]
    const vfss = [
      {
        vfsType: 'plain',
        config: {
          vfs: {
            read: (filespec) => {
              filespec = filespec.substring(filespec.lastIndexOf(':') + 1).substring(filespec.lastIndexOf('$') + 1)
              return seed.filter((f) => filespec === f.relative).reduce((accum, f) => {
                accum.contents = Buffer.from(f.contents)
                return accum
              }, {}).contents
            },

          },
        },
      },
      {
        vfsType: 'antora',
        config: {
          file: {
            src: {
              version: '4.5',
              component: 'component-a',
              module: 'module-a',
              family: 'page',
            },
          },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
        },
      },
    ]
    return vfss
  }

  ;[{
    type: 'global',
    f: (text, config) => {
      jsonpath.register(asciidoctor.Extensions, config)
      return asciidoctor.load(text)
    },
  },
  {
    type: 'registry',
    f: (text, config) => {
      const registry = jsonpath.register(asciidoctor.Extensions.create(), config)
      return asciidoctor.load(text, { extension_registry: registry })
    },
  }].forEach(({ type, f }) => {
    prepareVfss([
      {
        version: '4.5',
        family: 'page',
        relative: 'page-a.adoc',
        contents: `This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.
`,
      },
      {
        version: '4.5',
        family: 'example',
        relative: 'data1.json',
        contents: `{
"rows": [
  {
    "name": "foo",
    "description": "fooish",
    "uniqueish": 1
  },
  {
    "name": "bar",
    "description": "barish",
    "uniqueish": 2
  }
  ],
  "object": {
    "field1": {
      "p1": "a",
      "p2": "b",
      "uniqueish": 1
    },
    "field2": {
      "p1": "c",
      "p2": "d",
      "uniqueish": 1
    },
    "field3": {
      "p1": "e",
      "p2": "f",
      "uniqueish": 2
    }
  }
}`,
      },
      //Content presumably AL2 licensed, from Apache Camel.
      {
        version: '4.5',
        family: 'example',
        relative: 'data2.json',
        contents: `{
  "component": {
    "kind": "component",
    "name": "activemq",
    "title": "ActiveMQ",
    "description": "Send messages to (or consume from) Apache ActiveMQ. This component extends the Camel JMS component.",
    "deprecated": false,
    "firstVersion": "1.0.0",
    "label": "messaging",
    "javaType": "org.apache.camel.component.activemq.ActiveMQComponent",
    "supportLevel": "Stable",
    "groupId": "org.apache.camel",
    "artifactId": "camel-activemq",
    "version": "3.5.0-SNAPSHOT",
    "scheme": "activemq",
    "extendsScheme": "jms",
    "syntax": "activemq:destinationType:destinationName",
    "async": true,
    "consumerOnly": false,
    "producerOnly": false,
    "lenientProperties": false
  },
  "componentProperties": {
    "brokerURL": { "kind": "property", "displayName": "Broker URL", "group": "common", "label": "common", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "deprecationNote": "", "secret": false, "description": "Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)" },
    "clientId": { "kind": "property", "displayName": "Client Id", "group": "common", "label": "", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead." },
    "connectionFactory": { "kind": "property", "displayName": "Connection Factory", "group": "common", "label": "", "required": false, "type": "object", "javaType": "javax.jms.ConnectionFactory", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "The connection factory to be use. A connection factory must be configured either on the component or endpoint." },
    "disableReplyTo": { "kind": "property", "displayName": "Disable Reply To", "group": "common", "label": "", "required": false, "type": "boolean", "javaType": "boolean", "deprecated": false, "secret": false, "defaultValue": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Specifies whether Camel ignores the JMSReplyTo header in messages. If true, Camel does not send a reply back to the destination specified in the JMSReplyTo header. You can use this option if you want Camel to consume from a route and you do not want Camel to automatically send back a reply message because another component in your code handles the reply message. You can also use this option if you want to use Camel as a proxy between different message brokers and you want to route message from one system to another." },
    "durableSubscriptionName": { "kind": "property", "displayName": "Durable Subscription Name", "group": "common", "label": "", "required": false, "type": "string", "javaType": "java.lang.String", "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "The durable subscriber name for specifying durable topic subscriptions. The clientId option must be configured as well." },
    "jmsMessageType": { "kind": "property", "displayName": "Jms Message Type", "group": "common", "label": "", "required": false, "type": "object", "javaType": "org.apache.camel.component.jms.JmsMessageType", "enum": [ "Bytes", "Map", "Object", "Stream", "Text" ], "deprecated": false, "secret": false, "configurationClass": "org.apache.camel.component.jms.JmsConfiguration", "configurationField": "configuration", "description": "Allows you to force the use of a specific javax.jms.Message implementation for sending JMS messages. Possible values are: Bytes, Map, Object, Stream, Text. By default, Camel would determine which JMS message type to use from the In body type. This option allows you to specify it." }
  }
}
`,
      },
    ]).forEach(({ vfsType, config }) => {
      ;['', 'query='].forEach((query) => {
        ;['', 'cellformats='].forEach((cellformats) => {
          describe('jsonpathTableMacro tests', () => {
            it(`simple table test, ${type}, ${vfsType}`, () => {
              const doc = f(`

[cols='1,2',options="header"]
|===
|name
|description
|===

jsonpathTable::example$data1.json[${query}'$.rows[*]', ${cellformats}'name,description']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 66.6667%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">foo</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">fooish</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">bar</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">barish</p></td>
</tr>
</tbody>
</table>`)
            })

            it(`templated table test, ${type}, ${vfsType}`, () => {
              const doc = f(`

[cols='1,2',options="header"]
|===
|name
|description
|===

jsonpathTable::example$data1.json[${query}'$.rows[*]', ${cellformats}'the \${name} described by,the description \${description}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 33.3333%;">
<col style="width: 66.6667%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">description</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the foo described by</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description fooish</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">the bar described by</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">the description barish</p></td>
</tr>
</tbody>
</table>`)
            })

            it(`object table test, ${type}, ${vfsType}`, () => {
              const doc = f(`

[cols='1,2,3',options="header"]
|===
|name
|p1
|p2
|===

jsonpathTable::example$data1.json[${query}'nodes$.object.*', ${cellformats}'\${path[2]},\${value.p1},\${value.p2}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 16.6666%;">
<col style="width: 33.3333%;">
<col style="width: 50.0001%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">name</th>
<th class="tableblock halign-left valign-top">p1</th>
<th class="tableblock halign-left valign-top">p2</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field1</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">a</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">b</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field2</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">c</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">d</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock">field3</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">e</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">f</p></td>
</tr>
</tbody>
</table>`)
            })

            it(`camel object table test, ${type}, ${vfsType}`, () => {
              const doc = f(`

[width="100%",cols="2,5,^1,2",options="header"]
|===
| Name | Description | Default | Type
|===

jsonpathTable::example$data2.json[${query}'nodes$.componentProperties.*', ${cellformats}'*\${path[2]}* (\${value.group}),\${value.description},\${value.default || ""},\${value.type}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<table class="tableblock frame-all grid-all stretch">
<colgroup>
<col style="width: 20%;">
<col style="width: 50%;">
<col style="width: 10%;">
<col style="width: 20%;">
</colgroup>
<thead>
<tr>
<th class="tableblock halign-left valign-top">Name</th>
<th class="tableblock halign-left valign-top">Description</th>
<th class="tableblock halign-center valign-top">Default</th>
<th class="tableblock halign-left valign-top">Type</th>
</tr>
</thead>
<tbody>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>brokerURL</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">string</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>clientId</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">string</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>connectionFactory</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">The connection factory to be use. A connection factory must be configured either on the component or endpoint.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">object</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>disableReplyTo</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Specifies whether Camel ignores the JMSReplyTo header in messages. If true, Camel does not send a reply back to the destination specified in the JMSReplyTo header. You can use this option if you want Camel to consume from a route and you do not want Camel to automatically send back a reply message because another component in your code handles the reply message. You can also use this option if you want to use Camel as a proxy between different message brokers and you want to route message from one system to another.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">boolean</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>durableSubscriptionName</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">The durable subscriber name for specifying durable topic subscriptions. The clientId option must be configured as well.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">string</p></td>
</tr>
<tr>
<td class="tableblock halign-left valign-top"><p class="tableblock"><strong>jmsMessageType</strong> (common)</p></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">Allows you to force the use of a specific javax.jms.Message implementation for sending JMS messages. Possible values are: Bytes, Map, Object, Stream, Text. By default, Camel would determine which JMS message type to use from the In body type. This option allows you to specify it.</p></td>
<td class="tableblock halign-center valign-top"></td>
<td class="tableblock halign-left valign-top"><p class="tableblock">object</p></td>
</tr>
</tbody>
</table>`)
            })
          })
        })
        ;['', 'format='].forEach((format) => {
          describe('jsonpathListMacro tests', () => {
            it('simple ulist array test', () => {
              const doc = f(`

jsonpathList::example$data1.json[${query}'$.rows[*]', ${format}'\${name}: \${description}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="ulist">
<ul>
<li>
<p>foo: fooish</p>
</li>
<li>
<p>bar: barish</p>
</li>
</ul>
</div>`)
            })

            it('simple ulist object test', () => {
              const doc = f(`

jsonpathList::example$data1.json[${query}'nodes$.object.*', ${format}'\${path[2]}: \${value.p1} and \${value.p2}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="ulist">
<ul>
<li>
<p>field1: a and b</p>
</li>
<li>
<p>field2: c and d</p>
</li>
<li>
<p>field3: e and f</p>
</li>
</ul>
</div>`)
            })

            it('simple olist array test', () => {
              const doc = f(`

jsonpathOrderedList::example$data1.json[${query}'$.rows[*]', ${format}'\${name}: \${description}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="olist">
<ol class="">
<li>
<p>foo: fooish</p>
</li>
<li>
<p>bar: barish</p>
</li>
</ol>
</div>`)
            })

            it('simple olist object test', () => {
              const doc = f(`

jsonpathOrderedList::example$data1.json[${query}'nodes$.object.*', ${format}'\${path[2]}: \${value.p1} and \${value.p2}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="olist">
<ol class="">
<li>
<p>field1: a and b</p>
</li>
<li>
<p>field2: c and d</p>
</li>
<li>
<p>field3: e and f</p>
</li>
</ol>
</div>`)
            })
          })
          describe('jsonpathUniqueCountMacro tests', () => {
            it('simple unique count array test', () => {
              const doc = f(`

jsonpathUniqueCount:example$data1.json[${query}'$.rows[*\\]', ${format}'uniqueish']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
            })

            it('simple unique count object test', () => {
              const doc = f(`

jsonpathUniqueCount:example$data1.json[${query}'nodes$.object.*', ${format}'\${value.uniqueish}']

`, config)
              const html = doc.convert()
              expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
            })
          })
        })
        describe('jsonpathCountMacro tests', () => {
          it('simple count array test', () => {
            const doc = f(`

jsonpathCount:example$data1.json[${query}'$.rows[*\\]']

`, config)
            const html = doc.convert()
            expect(html).to.equal(`<div class="paragraph">
<p>2</p>
</div>`)
          })

          it('simple count object test', () => {
            const doc = f(`

jsonpathCount:example$data1.json[${query}'nodes$.object.*']

`, config)
            const html = doc.convert()
            expect(html).to.equal(`<div class="paragraph">
<p>3</p>
</div>`)
          })
        })
        ;['', 'subjectformat='].forEach((subjectformat) => {
          ;['', 'descriptionformat='].forEach((descriptionformat) => {
            describe('jsonpathDListMacro tests', () => {
              it('simple dlist array test', () => {
                const doc = f(`

jsonpathDescriptionList::example$data1.json[${query}'$.rows[*]', ${subjectformat}'\${name}', ${descriptionformat}'\${description}']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1">foo</dt>
<dd>
<p>fooish</p>
</dd>
<dt class="hdlist1">bar</dt>
<dd>
<p>barish</p>
</dd>
</dl>
</div>`)
              })

              it('simple dlist object test', () => {
                const doc = f(`

jsonpathDescriptionList::example$data1.json[${query}'nodes$.object.*', ${subjectformat}'\${path[2]}', ${descriptionformat}'\${value.p1} and \${value.p2}']

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="dlist">
<dl>
<dt class="hdlist1">field1</dt>
<dd>
<p>a and b</p>
</dd>
<dt class="hdlist1">field2</dt>
<dd>
<p>c and d</p>
</dd>
<dt class="hdlist1">field3</dt>
<dd>
<p>e and f</p>
</dd>
</dl>
</div>`)
              })
            })
          })
        })
        ;['', 'formats='].forEach((formats) => {
          ;['', 'target='].forEach((target) => {
            describe('jsonpathBlock tests', () => {
              it('simple block array test', () => {
                const doc = f(`

[jsonpathBlock, ${target}example$data1.json, ${query}'$.rows[*]', 'name,description,uniqueish']
----
== Section {name}

A description: {description} that is uniquely {uniqueish}
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect1">
<h2 id="_section_foo">Section foo</h2>
<div class="sectionbody">
<div class="paragraph">
<p>A description: fooish that is uniquely 1</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_bar">Section bar</h2>
<div class="sectionbody">
<div class="paragraph">
<p>A description: barish that is uniquely 2</p>
</div>
</div>
</div>`)
              })
              it('simple block object test', () => {
                const doc = f(`

[jsonpathBlock, ${target}example$data1.json, ${query}'nodes$.object.*', formats='name=path[2],p1=value.p1,p2=value.p2']
----

== Section {name}

Attribute p1 has value {p1}, whereas attribute p2 has value {p2}.
----

`, config)
                const html = doc.convert()
                expect(html).to.equal(`<div class="sect1">
<h2 id="_section_field1">Section field1</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value a, whereas attribute p2 has value b.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_field2">Section field2</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value c, whereas attribute p2 has value d.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_section_field3">Section field3</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Attribute p1 has value e, whereas attribute p2 has value f.</p>
</div>
</div>
</div>`)
              })
            })
          })
        })
      })

      describe('jsonpathAttributes include processor tests', () => {
        it('object properties', () => {
          const doc = f(`include::jsonpath$example$data2.json[query='$.component', formats='kind,name,mytitle=title,description']

kind: {kind}
name: {name}
title: {mytitle}
description: {description}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>kind: component
name: activemq
title: ActiveMQ
description: Send messages to (or consume from) Apache ActiveMQ. This component extends the Camel JMS component.</p>
</div>`)
        })

        it('simple name expressions', () => {
          const doc = f(`include::jsonpath$example$data2.json[query='nodes$.componentProperties.*', formats='path[2]=value.description']

brokerURL: {brokerurl}
clientId: {clientid}
connectionFactory: {connectionfactory}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>brokerURL: Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)
clientId: Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.
connectionFactory: The connection factory to be use. A connection factory must be configured either on the component or endpoint.</p>
</div>`)
        })

        it('complex name expressions', () => {
          const doc = f(`include::jsonpath$example$data2.json[query='nodes$.componentProperties.*', formats='\`\${path[2]}-description\`=value.description,\`\${path[2]}-javatype\`=value.javaType']

brokerURL description: {brokerurl-description}
brokerURL type: {brokerurl-javatype}
clientId description: {clientid-description}
clientId type: {clientid-javatype}
connectionFactory description: {connectionfactory-description}
connectionFactory type: {connectionfactory-javatype}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>brokerURL description: Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)
brokerURL type: java.lang.String
clientId description: Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.
clientId type: java.lang.String
connectionFactory description: The connection factory to be use. A connection factory must be configured either on the component or endpoint.
connectionFactory type: javax.jms.ConnectionFactory</p>
</div>`)
        })
        it('multiple uses', () => {
          const doc = f(`= My Page
include::jsonpath$example$data2.json[query='nodes$.componentProperties.*', formats='path[2]=value.description']
include::jsonpath$example$data2.json[query='nodes$.componentProperties.*', formats='\`\${path[2]}-description\`=value.description,\`\${path[2]}-javatype\`=value.javaType']
include::jsonpath$example$data2.json[query='$.component', formats='kind,name,mytitle=title,description']

:another-attribute3: foo

kind: {kind}
name: {name}
title: {mytitle}
description: {description}

brokerURL: {brokerurl}
clientId: {clientid}
connectionFactory: {connectionfactory}

brokerURL description: {brokerurl-description}
brokerURL type: {brokerurl-javatype}
clientId description: {clientid-description}
clientId type: {clientid-javatype}
connectionFactory description: {connectionfactory-description}
connectionFactory type: {connectionfactory-javatype}
`, config)
          const html = doc.convert()
          expect(html).to.equal(`<div class="paragraph">
<p>kind: component
name: activemq
title: ActiveMQ
description: Send messages to (or consume from) Apache ActiveMQ. This component extends the Camel JMS component.</p>
</div>
<div class="paragraph">
<p>brokerURL: Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)
clientId: Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.
connectionFactory: The connection factory to be use. A connection factory must be configured either on the component or endpoint.</p>
</div>
<div class="paragraph">
<p>brokerURL description: Sets the broker URL to use to connect to ActiveMQ. If none configured then localhost:61616 is used by default (however can be overridden by configuration from environment variables)
brokerURL type: java.lang.String
clientId description: Sets the JMS client ID to use. Note that this value, if specified, must be unique and can only be used by a single JMS connection instance. It is typically only required for durable topic subscriptions. If using Apache ActiveMQ you may prefer to use Virtual Topics instead.
clientId type: java.lang.String
connectionFactory description: The connection factory to be use. A connection factory must be configured either on the component or endpoint.
connectionFactory type: javax.jms.ConnectionFactory</p>
</div>`)
        })
      })
    })
  })
})
